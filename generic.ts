// [like,setLike]=useState=(1)

function useState() {
  let state: string | number;
  function getState() {
    return state;
  }
  function setState(x: string | number) {
    state = x;
  }
  return { getState, setState };
}

let like_useState = useState();

like_useState.setState(2);

console.log(like_useState.getState());

function useState_generic<T extends number | string>() {
  let state: T;
  function getState() {
    return state;
  }
  function setState(x: T) {
    state = x;
  }
  return { getState, setState };
}

let num_useState = useState_generic<string>();
num_useState.setState("2");

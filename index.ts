console.log("Hello");

let username = "alice";
//  ngầm định

// username = true;
// username = 112;

// typescript = type + javascript

// type : loại dữ liệu

// varible:type = value
// primitive value
let account: string = "admin123";
let userAge: number = 2;
userAge = 3;
// userAge = "3";
let isHoliday: boolean = true;
isHoliday = false;
// isHoliday = "yes";
let isMarried: null = null;

let is_married: undefined = undefined;

// reference value : object, array

// interface: dùng để định dạng format của object, giúp ràng buộc giữ liệu
interface Todo {
  id: number;
  name: string;
  isComplete: boolean;
}

interface NewTodo extends Todo {
  desc?: string; //optional property
}
let todo1: Todo = {
  id: 2,
  name: "Làm dự án cuối khoá",
  isComplete: false,
};
let todo2: Todo = {
  id: 3,
  name: "Làm capstone",
  isComplete: false,
};
let todo3: NewTodo = {
  id: 4,
  name: "Làm bài tập",
  isComplete: false,
  // desc: "Làm bài tập vui quá",
};

// type: cũng để định format object

type User = {
  id: number;
  name: string;
  age: number;
};

let user1: User = {
  id: 11,
  name: "alice",
  age: 2,
};

// array
// let colors: string[] = ["black", "blue", "green"];
let colors: Array<string> = ["black", "blue", "green"];
// Array<string> generic

colors.push("white");
// colors.push(2); error

let todos: Todo[] = [{ id: 2, name: "Homework", isComplete: true }];
// advanced type

// union type: Cho phép 1 biến có thể chứa được nhiều loại dữ liệu cho trước
type ResponseTodoBE = null | Todo;

type ResponseAgeBE = null | number;

let userAge1: ResponseAgeBE = null;
userAge1 = 2;
// userAge1 = "2";

let todoDetail: ResponseTodoBE = null;
todoDetail = {
  id: 2,
  name: "Lau nhà",
  isComplete: false,
};
// let detail = null;

// detail = {
//   id: 2,
// };

enum GenderType {
  male = 0,
  female = 1,
}

interface SV {
  id: string;
  name: string;
  gender: GenderType;
}

let sv1: SV = {
  id: "11",
  name: "Bob",
  gender: GenderType.female,
};

let value: any = "alice";
value = true;
value = 112;

interface Car {
  id: number;
  name: string;
  price: number;
  desc: string;
}

// Partial chuyển từ require thành optional
let introduceCar = (carInfor: Partial<Car>) => {
  console.log("Thông tin xe");
  console.log(carInfor.name, carInfor.price);
};
introduceCar({
  name: "Vinfast",
  price: 2,
});

// Readonly không cho edit value của object
let car1: Readonly<Car> = {
  id: 222,
  name: "Range Rover",
  price: 1,
  desc: "Đẹp",
};
// car1.desc = "Xấu ";

interface TodoProps {
  id: number;
  name: string;
  desc?: string;
}
// Required biến optional thành require
let myTodo: Required<TodoProps> = {
  id: 2,
  name: "Làm việc nhà",
  desc: "15p nữa làm sau",
};

// overloading và overiding trong js,ts

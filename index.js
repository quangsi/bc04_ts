console.log("Hello");
var username = "alice";
//  ngầm định
// username = true;
// username = 112;
// typescript = type + javascript
// type : loại dữ liệu
// varible:type = value
// primitive value
var account = "admin123";
var userAge = 2;
userAge = 3;
// userAge = "3";
var isHoliday = true;
isHoliday = false;
// isHoliday = "yes";
var isMarried = null;
var is_married = undefined;
var todo1 = {
  id: 2,
  name: "Làm dự án cuối khoá",
  isComplete: false,
};
var todo2 = {
  id: 3,
  name: "Làm capstone",
  isComplete: false,
};
var todo3 = {
  id: 4,
  name: "Làm bài tập",
  isComplete: false,
};
var user1 = {
  id: 11,
  name: "alice",
  age: 2,
};
// array
// let colors: string[] = ["black", "blue", "green"];
var colors = ["black", "blue", "green"];
// Array<string> generic
colors.push("white");
// colors.push(2); error
var todos = [{ id: 2, name: "Homework", isComplete: true }];
var userAge1 = null;
userAge1 = 2;
// userAge1 = "2";
var todoDetail = null;
todoDetail = {
  id: 2,
  name: "Lau nhà",
  isComplete: false,
};
// let detail = null;
// detail = {
//   id: 2,
// };

// js

const user = "alice";
user = "bob";

const sv = {
  name: "alice",
};

const colors = [];
colors.push("Red");
colors = "alice";

sv.name = "bob";

sv = "bob";

// gross        net

